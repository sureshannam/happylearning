package com.happylearning.controller;

import com.happylearning.model.Department;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class DepartmentController {
    @GetMapping("/department")
    public ResponseEntity<Department> getDepartmentDetails(@RequestParam String name){
        Department department = new Department();
        if("Bhagat".equals(name)){
            department.setDepartment("Java");
        } else {
            department.setDepartment("Spring");
        }
        return new ResponseEntity<>(department, HttpStatus.OK);
    }
}
